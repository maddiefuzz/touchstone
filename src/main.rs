extern crate sdl2; 

use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

use std::time::Duration;
use std::path::Path;

use sdl2::ttf::*;

static SCREEN_WIDTH: u32 = 480;
static SCREEN_HEIGHT: u32 = 320;

macro_rules! rect(
    ($x:expr, $y:expr, $w:expr, $h:expr) => (
        Rect::new($x as i32, $y as i32, $w as u32, $h as u32)
    )
);

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
 
    let window = video_subsystem.window("rust-sdl2 demo", 480, 320)
        .position_centered()
        .build()
        .unwrap();
 
    let mut canvas = window.into_canvas().build().unwrap();
    let texture_creator = canvas.texture_creator();

    // Font stuff
    let ttf_context = sdl2::ttf::init().map_err(|e| e.to_string()).unwrap();
    let font_path = Path::new("assets/handel_gothic_regular.ttf");
    let mut font = ttf_context.load_font(font_path, 128).unwrap();

    let surface = font
        .render("LIFE SUPPORT")
        .blended(Color::RGBA(200, 200, 200, 255))
        .map_err(|e| e.to_string()).unwrap();

    let texture = texture_creator
        .create_texture_from_surface(&surface)
        .map_err(|e| e.to_string()).unwrap();
    
    let surface2 = font
        .render("TEROK NOR")
        .blended(Color::RGBA(220, 220, 220, 255))
        .map_err(|e| e.to_string()).unwrap();

    let texture2 = texture_creator
        .create_texture_from_surface(&surface2)
        .map_err(|e| e.to_string()).unwrap();

    let target = rect!(90, 58, 300, 32);
    let target2 = rect!(110, 20, 260, 32);
 
    canvas.set_draw_color(Color::RGB(0, 255, 255));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut i = 0;
    'running: loop {
        i = (i + 1) % 255;
        canvas.set_draw_color(Color::RGB(48, 39, 49));
        canvas.clear();
        canvas.copy(&texture, None, Some(target)).unwrap();
        canvas.copy(&texture2, None, Some(target2)).unwrap();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                _ => {}
            }
        }
        // The rest of the game loop goes here...

        canvas.present();
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}
